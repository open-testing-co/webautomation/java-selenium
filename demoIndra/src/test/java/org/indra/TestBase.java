package org.indra;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestBase {
    public WebDriver driver;

    @Before
    public void antesDeCadaPrueba(){
        driver = new ChromeDriver();

    }

    @After
    public void despuesDeCadaPrueba(){
        driver.quit();
    }
}
