package org.indra;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginTest extends TestBase{


    @BeforeClass
    public static void antesDeTodasLasPruebas(){
        System.out.println("Antes que todos");
    }

    @Test
    public void loginInvalido(){
        driver.get("https://www.saucedemo.com");
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.id("password")).sendKeys("Clave incorrecta");
        driver.findElement(By.className("btn_action")).click();

        String textError=driver.findElement(By.xpath("//*[@data-test='error']")).getText();
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service",textError);
    }

    @Test
    public void loginSinPassword(){
        driver.get("https://www.saucedemo.com");
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.className("btn_action")).click();

        String textError=driver.findElement(By.xpath("//*[@data-test='error']")).getText();
        Assert.assertEquals("Epic sadface: Password is required",textError);
    }

    @Test
    public void loginValido(){
        driver.get("https://www.saucedemo.com");
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.id("password")).sendKeys("secret_sauce");
        driver.findElement(By.className("btn_action")).click();

        boolean carritoVisible=driver.findElement(By.id("shopping_cart_container")).isDisplayed();
        Assert.assertTrue("El usuario no se loguio exitosamente",carritoVisible);
    }


}
